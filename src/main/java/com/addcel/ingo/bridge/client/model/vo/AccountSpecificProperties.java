package com.addcel.ingo.bridge.client.model.vo;

public class AccountSpecificProperties {

	private String cardExpiryDate;

	public String getCardExpiryDate() {
		return cardExpiryDate;
	}

	public void setCardExpiryDate(String cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}
	
	
}
