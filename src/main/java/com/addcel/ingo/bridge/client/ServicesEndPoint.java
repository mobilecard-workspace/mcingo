package com.addcel.ingo.bridge.client;

public class ServicesEndPoint {

	public static final String server = "http://localhost:80";
	//PROD = http://192.168.75.51:8081
	//QA  http://192.168.75.53:8081
	
	public static final String getSession = server + "/IngoBridgeWS/getSession"; //192.168.75.53
	public static final String FindCustomer = server + "/IngoBridgeWS/FindCustomer";
	public static final String GetRegisteredCards = server + "/IngoBridgeWS/GetRegisteredCards";
	public static final String EnrollCustomer = server + "/IngoBridgeWS/EnrollCustomer";
	public static final String AddCustomerAttributes = "https://uat2.spykemobile.net:8443/SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddCustomerAttributes";
	public static final String AddOrUpdateCard = server + "/IngoBridgeWS/AddOrUpdateCard";
	public static final String AddOrUpdateTokenizedCard  = "https://uat2.spykemobile.net:8443/SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddOrUpdateTokenizedCard";
	public static final String DeleteCard = "https://uat2.spykemobile.net:8443/SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/DeleteCard";
	public static final String AuthenticateOBO = server + "/IngoBridgeWS/AuthenticateOBO";
	public static final String AddSessionAttributes  = "https://uat2.spykemobile.net:8443/SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddSessionAttributes";
	public static final String UpdateAccount = "https://uat2.spykemobile.net:8443/SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/UpdateAccount";
	public static final String GetTransactionHistory = "https://uat2.spykemobile.net:8443/SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/GetTransactionHistory";
	
	
	public static final String blackstone = server + "/MCBlackstoneServices/mobilecard/blackstone/send/payment";
	
	/**
	 * {
"partnerConnectId":"Api.ConnectId.Android.050516.192907@chexar.com",
"partnerConnectToken":"YzAxNzBkMGYtZTNjOS00YjM2LTg2ZjUtOTNkMWFkOWI2MmZk"
}
	 */
	
}
