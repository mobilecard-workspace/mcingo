package com.addcel.ingo.mcingo.spring.component;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.addcel.ingo.bridge.client.model.vo.LoginRequest;


@Aspect
@Component
public class LogAspect {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LogAspect.class);
	
	  @Before("execution(* com.addcel.ingo.mcingo.services.MCIngoServices.getSessionData(..))")
	    public void logBefore(JoinPoint joinPoint) {
		try{
				LOGGER.info("logBefore");
			
			  LOGGER.info("Log before in: " + joinPoint.getSignature().getName());
			 // Object args = joinPoint.getArgs();
			 
			  Object[] signatureArgs = joinPoint.getArgs();
			   for (Object signatureArg: signatureArgs) {
				   LOGGER.info("Arg: " + ((LoginRequest)signatureArg).getDeviceId());
			      
			   }
			}catch(Exception ex){
				LOGGER.error("ERROR AL PROCESAR ARGUMENTOS ", ex);
			}
		  
	    }
	  
	  @AfterThrowing(pointcut = "execution(* com.addcel.ingo.mcingo.services.MCIngoServices.getSessionData(..))", throwing = "exception")
	    public void logAfterThrowing(JoinPoint joinPoint, Throwable exception) {
		  LOGGER.info("logAfterThrowing");
		  LOGGER.info("Log in: " + joinPoint.getSignature().getName());
		  LOGGER.info("- And thrown exception is: " + exception.getMessage());
	    }

}
