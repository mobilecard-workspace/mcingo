package com.addcel.ingo.mcingo.spring.model;

import java.util.List;

import com.addcel.ingo.mcingo.mybatis.model.vo.Estados;


public class EstadosResponse {

	List<Estados> estados;
	private int idError ;
	private String mensajeError;
	
	public EstadosResponse() {
		// TODO Auto-generated constructor stub
	}

	public List<Estados> getEstados() {
		return estados;
	}

	public void setEstados(List<Estados> estados) {
		this.estados = estados;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	
	
	
}
