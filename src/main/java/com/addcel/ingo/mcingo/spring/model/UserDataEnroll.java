package com.addcel.ingo.mcingo.spring.model;

import com.addcel.ingo.bridge.client.model.vo.SessionResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDataEnroll extends SessionResponse{

	private int state; 
	private String city; 
	private String address;
	private String zip;
	
	public UserDataEnroll() {
		// TODO Auto-generated constructor stub
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
	
	
}
