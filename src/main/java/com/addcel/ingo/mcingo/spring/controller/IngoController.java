package com.addcel.ingo.mcingo.spring.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateCardRequestMC;
import com.addcel.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.ingo.bridge.client.model.vo.EnrollCustomerMC;
import com.addcel.ingo.bridge.client.model.vo.FindCustomerRequestMC;
import com.addcel.ingo.bridge.client.model.vo.GetRegisteredCardsRequestMC;
import com.addcel.ingo.bridge.client.model.vo.Response;
import com.addcel.ingo.bridge.client.model.vo.SessionResponse;
import com.addcel.ingo.mcingo.services.IngoServices;
import com.addcel.ingo.mcingo.spring.model.EstadosResponse;
import com.addcel.ingo.mcingo.spring.model.SeRequest;
import com.addcel.ingo.mcingo.spring.model.UserDataEnroll;
@Controller
public class IngoController {

	/*private static final Logger LOGGER = LoggerFactory.getLogger(IngoController.class);
	
	@RequestMapping(value="/{idApp}/{idioma}/getSessionData",  method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<SessionResponse> getSessionData(@RequestBody LoginRequest login, @PathVariable("idApp") int idApp ){
		return new ResponseEntity<SessionResponse>(new SessionResponse(),HttpStatus.OK);
	}*/
	
private static final Logger LOGGER = LoggerFactory.getLogger(IngoController.class);
	
	@Autowired
	private IngoServices IServices;
	
	@RequestMapping(value="/{idApp}/{idioma}/getSessionData",  method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<SessionResponse> getSessionData(@RequestBody SeRequest session, @PathVariable("idApp") int idApp,@PathVariable("idioma") String idioma,HttpServletRequest req ){
		LOGGER.debug("PETIONCION DESDE IP: " + req.getRemoteAddr());
		return new ResponseEntity<SessionResponse>(IServices.getSessionData(session, idioma, idApp),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{idApp}/{idioma}/EnrollCustomer", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<UserDataEnroll> EnrollCustomer(@RequestBody EnrollCustomerMC enrollMC,@PathVariable("idApp") int idApp,@PathVariable("idioma") String idioma,HttpServletRequest req ){
		LOGGER.debug("Iniciando preoceso de enrolamiento");
		LOGGER.debug("PETIONCION DESDE IP: " + req.getRemoteAddr());
		UserDataEnroll response = IServices.EnrollCustomer(enrollMC, idioma, idApp);
		return  new ResponseEntity<UserDataEnroll>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{idApp}/{idioma}/AddOrUpdateCard",method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Response>  AddOrUpdateCard(@RequestBody AddOrUpdateCardRequestMC addOrUpdateRequest,@PathVariable("idApp") int idApp,@PathVariable("idioma") String idioma,HttpServletRequest req ){
		LOGGER.debug("PETIONCION DESDE IP: " + req.getRemoteAddr());
		Response response = IServices.AddOrUpdateCard(addOrUpdateRequest,idioma,idApp);
		return  new ResponseEntity<Response>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value="/{idApp}/{idioma}/FindCustomer", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CustomerResponse> FindCustomer(@RequestBody FindCustomerRequestMC findcustomer,@PathVariable("idApp") int idApp,@PathVariable("idioma") String idioma,HttpServletRequest req ){
		LOGGER.debug("PETIONCION DESDE IP: " + req.getRemoteAddr());
		CustomerResponse response = IServices.FindCustomer(findcustomer,idioma,idApp);
		return  new ResponseEntity<CustomerResponse>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value="/{idApp}/{idioma}/GetRegisteredCards", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CardsResponse> GetRegisteredCards(@RequestBody GetRegisteredCardsRequestMC request,@PathVariable("idApp") int idApp,@PathVariable("idioma") String idioma,HttpServletRequest req ){
		LOGGER.debug("PETIONCION DESDE IP: " + req.getRemoteAddr());
		CardsResponse response = IServices.GetRegisteredCards(request,idioma,idApp);
		return  new ResponseEntity<CardsResponse>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value = "{idApp}/{idioma}/{idpais}/estados", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<EstadosResponse> getEstados(@PathVariable int idApp,@PathVariable int idpais,@PathVariable String idioma,HttpServletRequest req ) {
		LOGGER.debug("PETIONCION DESDE IP: " + req.getRemoteAddr());
		return  new ResponseEntity<EstadosResponse>(IServices.getEstates(idpais,idApp,idioma),HttpStatus.OK);
	}

	
}
