package com.addcel.ingo.mcingo.mybatis.model.mapper;

import java.util.HashMap;

import org.apache.ibatis.annotations.Param;

import com.addcel.ingo.mcingo.mybatis.model.vo.BitacoraIngo;
import com.addcel.ingo.mcingo.mybatis.model.vo.BlacksPayData;
import com.addcel.ingo.mcingo.mybatis.model.vo.ClienteVO;
import com.addcel.ingo.mcingo.mybatis.model.vo.DataUser;
import com.addcel.ingo.mcingo.mybatis.model.vo.Estados;
import com.addcel.ingo.mcingo.mybatis.model.vo.MCCard;
import com.addcel.ingo.mcingo.mybatis.model.vo.ProjectMC;
import com.addcel.ingo.mcingo.mybatis.model.vo.Proveedor;
import com.addcel.ingo.mcingo.mybatis.model.vo.User;






public interface ServiceMapper {

	//public ClienteVO getClient(@Param(value = "user") String user, @Param(value = "pass") String pass);
	public User getCustumerID(@Param(value = "user") String user, @Param(value = "pass") String pass);
	public User getCustumerIDbyId(@Param(value = "user") long  user);
	public Integer mobilecardCard(long id_usuario);
	public String  getUser(HashMap datauser);
	public void updateCustomerId(User user);
	public void insertBitacoraIngo(BitacoraIngo bitacoraIngo);
	public BlacksPayData getPayBlackData(String id_ingo);
	public Proveedor getProveedor(String proveedor);
	public MCCard getMobilecardCard(long id_usuario);
	public Estados getState(@Param(value = "abb") String abb, @Param(value = "id") int id);
	public String getParameter(@Param(value = "parameter") String parameter);
	public void setUserLanguaje(@Param(value = "idi") String idi, @Param(value = "idUsuario") long idUsuario,@Param(value = "customer") String customer);
	public String getUserLanguanje(@Param(value = "customer") String customer);
	public ProjectMC getProjectMC( @Param(value = "cont") String cont,@Param(value = "recu") String recu);
	
}
