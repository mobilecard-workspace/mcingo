package com.addcel.ingo.mcingo.mybatis.model.vo;

public class BitacoraIngo {
	
	private String CustomerId;
	private String CancelledByDescription;
	private String CardBinNumber;
	private String CardId;
	private String CardNickName;
	private String CheckSubmissionDate;
	private String DeclineCode;
	private String DeclineReason;
	private String FundingDestinationIdentifier;
	private String FundsAvailableDate;
	private String LastFourOfCard;
	private String NotificationId;
	private String NotificationTypeDescription;
	private String PromoCode;
	private String PromoType;
	private String TransactionId;
	private String TransactionStatusDescription;
	private String TransactionTypeDescription;
	private String CancelledBy;
	private String Fee;
	private String FeeDeltaInCents;
	private String IsPpg;
	private String KeyedAmount;
	private String LoadAmount;
	private String NotificationType;
	private String PromoAmount;
	private String TransactionStatus;
	private String TransactionType;
	private double Commission;
	private java.sql.Date Date;
	private int statusCommission;
	private long idbitacora;
	
	
	public long getIdbitacora() {
		return idbitacora;
	}
	public void setIdbitacora(long idbitacora) {
		this.idbitacora = idbitacora;
	}
	public double getCommission() {
		return Commission;
	}
	public void setCommission(double commission) {
		Commission = commission;
	}
	public java.sql.Date getDate() {
		return Date;
	}
	public void setDate(java.sql.Date date) {
		Date = date;
	}
	public int getStatusCommission() {
		return statusCommission;
	}
	public void setStatusCommission(int statusCommission) {
		this.statusCommission = statusCommission;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getCancelledByDescription() {
		return CancelledByDescription;
	}
	public void setCancelledByDescription(String cancelledByDescription) {
		CancelledByDescription = cancelledByDescription;
	}
	public String getCardBinNumber() {
		return CardBinNumber;
	}
	public void setCardBinNumber(String cardBinNumber) {
		CardBinNumber = cardBinNumber;
	}
	public String getCardId() {
		return CardId;
	}
	public void setCardId(String cardId) {
		CardId = cardId;
	}
	public String getCardNickName() {
		return CardNickName;
	}
	public void setCardNickName(String cardNickName) {
		CardNickName = cardNickName;
	}
	public String getCheckSubmissionDate() {
		return CheckSubmissionDate;
	}
	public void setCheckSubmissionDate(String checkSubmissionDate) {
		CheckSubmissionDate = checkSubmissionDate;
	}
	public String getDeclineCode() {
		return DeclineCode;
	}
	public void setDeclineCode(String declineCode) {
		DeclineCode = declineCode;
	}
	public String getDeclineReason() {
		return DeclineReason;
	}
	public void setDeclineReason(String declineReason) {
		DeclineReason = declineReason;
	}
	public String getFundingDestinationIdentifier() {
		return FundingDestinationIdentifier;
	}
	public void setFundingDestinationIdentifier(String fundingDestinationIdentifier) {
		FundingDestinationIdentifier = fundingDestinationIdentifier;
	}
	public String getFundsAvailableDate() {
		return FundsAvailableDate;
	}
	public void setFundsAvailableDate(String fundsAvailableDate) {
		FundsAvailableDate = fundsAvailableDate;
	}
	public String getLastFourOfCard() {
		return LastFourOfCard;
	}
	public void setLastFourOfCard(String lastFourOfCard) {
		LastFourOfCard = lastFourOfCard;
	}
	public String getNotificationId() {
		return NotificationId;
	}
	public void setNotificationId(String notificationId) {
		NotificationId = notificationId;
	}
	public String getNotificationTypeDescription() {
		return NotificationTypeDescription;
	}
	public void setNotificationTypeDescription(String notificationTypeDescription) {
		NotificationTypeDescription = notificationTypeDescription;
	}
	public String getPromoCode() {
		return PromoCode;
	}
	public void setPromoCode(String promoCode) {
		PromoCode = promoCode;
	}
	public String getPromoType() {
		return PromoType;
	}
	public void setPromoType(String promoType) {
		PromoType = promoType;
	}
	public String getTransactionId() {
		return TransactionId;
	}
	public void setTransactionId(String transactionId) {
		TransactionId = transactionId;
	}
	public String getTransactionStatusDescription() {
		return TransactionStatusDescription;
	}
	public void setTransactionStatusDescription(String transactionStatusDescription) {
		TransactionStatusDescription = transactionStatusDescription;
	}
	public String getTransactionTypeDescription() {
		return TransactionTypeDescription;
	}
	public void setTransactionTypeDescription(String transactionTypeDescription) {
		TransactionTypeDescription = transactionTypeDescription;
	}
	public String getCancelledBy() {
		return CancelledBy;
	}
	public void setCancelledBy(String cancelledBy) {
		CancelledBy = cancelledBy;
	}
	public String getFee() {
		return Fee;
	}
	public void setFee(String fee) {
		Fee = fee;
	}
	public String getFeeDeltaInCents() {
		return FeeDeltaInCents;
	}
	public void setFeeDeltaInCents(String feeDeltaInCents) {
		FeeDeltaInCents = feeDeltaInCents;
	}
	public String getIsPpg() {
		return IsPpg;
	}
	public void setIsPpg(String isPpg) {
		IsPpg = isPpg;
	}
	public String getKeyedAmount() {
		return KeyedAmount;
	}
	public void setKeyedAmount(String keyedAmount) {
		KeyedAmount = keyedAmount;
	}
	public String getLoadAmount() {
		return LoadAmount;
	}
	public void setLoadAmount(String loadAmount) {
		LoadAmount = loadAmount;
	}
	public String getNotificationType() {
		return NotificationType;
	}
	public void setNotificationType(String notificationType) {
		NotificationType = notificationType;
	}
	public String getPromoAmount() {
		return PromoAmount;
	}
	public void setPromoAmount(String promoAmount) {
		PromoAmount = promoAmount;
	}
	public String getTransactionStatus() {
		return TransactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		TransactionStatus = transactionStatus;
	}
	public String getTransactionType() {
		return TransactionType;
	}
	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}
	
	

}
