package com.addcel.ingo.mcingo.mybatis.model.vo;

public class BlacksPayData {

	private Long id_usuario;
	private String imei;
	private String software;
	private String modelo;
	private String tipo;
	private String wkey;
	private String usr_cp;
	private String ct;
	private String vigencia;
	private String pan;
	private int idpais;
	private String eMail;
	private String usr_nombre;
	private String usr_apellido;
	
	
	public BlacksPayData() {
		// TODO Auto-generated constructor stub
	}
	
	public void setUsr_apellido(String usr_apellido) {
		this.usr_apellido = usr_apellido;
	}
	
	public void setUsr_nombre(String usr_nombre) {
		this.usr_nombre = usr_nombre;
	}
	
	public String getUsr_apellido() {
		return usr_apellido;
	}
	
	public String getUsr_nombre() {
		return usr_nombre;
	}
	
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	
	public String geteMail() {
		return eMail;
	}

	public void setIdpais(int idpais) {
		this.idpais = idpais;
	}
	
	public int getIdpais() {
		return idpais;
	}
	
	public String getVigencia() {
		return vigencia;
	}



	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}



	public String getPan() {
		return pan;
	}



	public void setPan(String pan) {
		this.pan = pan;
	}



	public Long getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getWkey() {
		return wkey;
	}

	public void setWkey(String wkey) {
		this.wkey = wkey;
	}

	public String getUsr_cp() {
		return usr_cp;
	}

	public void setUsr_cp(String usr_cp) {
		this.usr_cp = usr_cp;
	}

	public String getCt() {
		return ct;
	}

	public void setCt(String ct) {
		this.ct = ct;
	}
	
	
	
}
