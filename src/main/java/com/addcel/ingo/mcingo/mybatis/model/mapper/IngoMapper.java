package com.addcel.ingo.mcingo.mybatis.model.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.ingo.mcingo.mybatis.model.vo.BitacoraIngo;
import com.addcel.ingo.mcingo.mybatis.model.vo.BlacksPayData;
import com.addcel.ingo.mcingo.mybatis.model.vo.Estados;
import com.addcel.ingo.mcingo.mybatis.model.vo.MCCard;
import com.addcel.ingo.mcingo.mybatis.model.vo.ProjectMC;
import com.addcel.ingo.mcingo.mybatis.model.vo.Proveedor;
import com.addcel.ingo.mcingo.mybatis.model.vo.User;

public interface IngoMapper {
	
	public User getCustumerID(@Param(value = "user") String user, @Param(value = "pass") String pass);
	public User getCustumerIDbyId(@Param(value = "user") long  user, @Param(value = "idApp") int idApp);
	public User getUserByIdCustomer(@Param(value = "idIngo") String  idIngo, @Param(value = "idApp") int idApp);
	//public Integer mobilecardCard(long id_usuario);
	public String  getUser(HashMap datauser);
	public void updateCustomerId(@Param(value = "idUsuario") long idUsuario,@Param(value = "idingo") String idingo,@Param(value = "idstate") int idstate, @Param(value = "snn") String snn, @Param(value = "idApp") int idApp );
	
	public void insertBitacoraIngo(BitacoraIngo bitacoraIngo);
	public BlacksPayData getPayBlackData(String id_ingo);
	public Proveedor getProveedor(String proveedor);
	public MCCard getMobilecardCard(long id_usuario);
	public Estados getState(@Param(value = "abb") String abb, @Param(value = "id") int id,@Param(value = "idpais") int idpais);
	public String getParameter(@Param(value = "parameter") String parameter);
	public void setUserLanguaje(@Param(value = "idi") String idi, @Param(value = "idUsuario") long idUsuario,@Param(value = "customer") String customer);
	public String getUserLanguanje(@Param(value = "customer") String customer);
	public ProjectMC getProjectMC( @Param(value = "cont") String cont,@Param(value = "recu") String recu);
	List<Estados> getEstates(@Param(value = "idpais") int idpais, @Param(value = "id") int idestado,  @Param(value = "abb") String abbestado);

}
