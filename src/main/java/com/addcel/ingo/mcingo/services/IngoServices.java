package com.addcel.ingo.mcingo.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateCardRequest;
import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateCardRequestMC;
import com.addcel.ingo.bridge.client.model.vo.AuthenticateOBORequest;
import com.addcel.ingo.bridge.client.model.vo.AuthenticateOBOResponse;
import com.addcel.ingo.bridge.client.model.vo.AuthenticatePartnerResponse;
import com.addcel.ingo.bridge.client.model.vo.Card;
import com.addcel.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.ingo.bridge.client.model.vo.EnrollCustomerMC;
import com.addcel.ingo.bridge.client.model.vo.EnrollCustomerRequest;
import com.addcel.ingo.bridge.client.model.vo.FindCustomerRequest;
import com.addcel.ingo.bridge.client.model.vo.FindCustomerRequestMC;
import com.addcel.ingo.bridge.client.model.vo.GetRegisteredCardsRequest;
import com.addcel.ingo.bridge.client.model.vo.GetRegisteredCardsRequestMC;
import com.addcel.ingo.bridge.client.model.vo.LoginRequest;
import com.addcel.ingo.bridge.client.model.vo.Response;
import com.addcel.ingo.bridge.client.model.vo.SessionRequest;
import com.addcel.ingo.bridge.client.model.vo.SessionResponse;
import com.addcel.ingo.mcingo.mybatis.model.mapper.IngoMapper;
import com.addcel.ingo.mcingo.mybatis.model.mapper.ServiceMapper;
import com.addcel.ingo.mcingo.mybatis.model.vo.Estados;
import com.addcel.ingo.mcingo.mybatis.model.vo.MCCard;
import com.addcel.ingo.mcingo.mybatis.model.vo.Proveedor;
import com.addcel.ingo.mcingo.mybatis.model.vo.User;
import com.addcel.ingo.mcingo.spring.component.UtilsService;
import com.addcel.ingo.mcingo.spring.model.EstadosResponse;
import com.addcel.ingo.mcingo.spring.model.SeRequest;
import com.addcel.ingo.mcingo.spring.model.UserDataEnroll;
import com.addcel.ingo.mcingo.utils.UtilsMC;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

import crypto.Crypto;

///opt/cvs/mobilecard

@Service
public class IngoServices {

	private static final Logger LOGGER = LoggerFactory.getLogger(IngoServices.class);
	
	@Autowired
	private MCIngorequest mcIngorequest;
	
	@Autowired
	private IngoMapper mapper;
	
	private Gson gson = new Gson();
			
	public SessionResponse getSessionData(SeRequest s, String idioma, int idApp){
		
		SessionResponse response =  new SessionResponse();
		try{
			LOGGER.debug("[INICIANDO PROCESO DE DATOS SESION] " + idApp);
			
			Proveedor proveedor= mapper.getProveedor("INGO");
			
			if(proveedor.getPrv_status() == 0)
			{
				
				response.setCustomerId("");
				response.setErrorCode(503);
				if(idioma.equalsIgnoreCase("es"))
					response.setErrorMessage("");
				else
					response.setErrorMessage("We are improving this functionality. It is temporarily unavailable");
				response.setSsoToken("");
				response.setSessionId("");
				return response;
			}
			
			String customerId = null;
			SessionRequest sessionRequest = new SessionRequest();				

			User user = mapper.getCustumerIDbyId(s.getIdUsuario(), idApp);
			
			CardsResponse  activeIngo = RegisteredCard(user,s.getDeviceId(),idioma);
				
				if(activeIngo.getErrorCode()== 0 ) //&& activeIngo.getCards().size() > 0
				{
						
						AuthenticatePartnerResponse  authenticatePartnerResponse = mcIngorequest.getSession(sessionRequest, s.getDeviceId(), s.getPlataforma());
						response.setSessionId(authenticatePartnerResponse.getSessionId());
						
						if(response.getSessionId()== null || response.getSessionId().isEmpty())
						{
							response.setErrorCode(authenticatePartnerResponse.getErrorCode());
							response.setErrorMessage(authenticatePartnerResponse.getErrorMessage());
						}
						else
						{
						
							customerId = user.getId_ingo();
							response.setCustomerId(customerId);
							
							if(response.getCustomerId() == null || response.getCustomerId().isEmpty())
							{
								response.setErrorCode(100);
								response.setErrorMessage("Error fetching database data: customerId is null or empty" );
							}
							else
							{
								AuthenticateOBORequest authenticateOBORequest = new AuthenticateOBORequest();
								authenticateOBORequest.setCustomerId(customerId);
								AuthenticateOBOResponse  authenticateOBOResponse  = mcIngorequest.AuthenticateOBO(authenticateOBORequest, s.getDeviceId());
								response.setSsoToken(authenticateOBOResponse.getSsoToken());
								response.setErrorCode(authenticateOBOResponse.getErrorCode());
								response.setErrorMessage(authenticateOBOResponse.getErrorMessage());
								mapper.setUserLanguaje(idioma, user.getId_usuario(),customerId); //login.getIdioma().trim()
							}
						}
			  }
				else
				{
					response.setCustomerId("");
					response.setErrorCode(activeIngo.getErrorCode());
					response.setErrorMessage(activeIngo.getErrorMessage());
					response.setSessionId("");
					response.setSsoToken("");
				}
			
			//EnrollCustomer("infernodidante","Ak0ynTun/y9it4Avvgthdg==");
			//getUser("artcervs","TXgw59dS670mwLAsapjA1w==");
			LOGGER.debug("CustomerId: " + response.getCustomerId());
			LOGGER.debug("SessionId: " + response.getSessionId());
			LOGGER.debug("SsoToken: "+response.getSsoToken());
			LOGGER.debug("ErrorMessage: " + response.getErrorMessage());
			LOGGER.debug("ErrorCode: " + response.getErrorCode());
			return response;
			
		}catch(Exception  ex){
			LOGGER.error("[ERROR AL OBTENER DATOS DE SESION]", ex);
			response.setCustomerId("");
			response.setErrorCode(80);
			if(idioma.equalsIgnoreCase("es"))
				response.setErrorMessage("Error al obtener datos de session, intente mas tarde");
			else
				response.setErrorMessage("Error Processing Data");
			response.setSessionId("");
			response.setSsoToken("");
			//ex.printStackTrace();
			return response;
		}
	}
	
	
	/*
	 * Checar si el usuario cuanta con tarjeta enrola en ingo
	 */
	private CardsResponse RegisteredCard(User user, String deviceId, String idioma){
		
		CardsResponse response = new CardsResponse();
		try{
			
			Proveedor proveedor= mapper.getProveedor("INGO");
			if(proveedor.getPrv_status() == 0)
			{
				response.setCards(new ArrayList<Card>());
				response.setErrorCode(503);
				if(idioma.equalsIgnoreCase("es"))
					response.setErrorMessage("");
				else
					response.setErrorMessage("We are improving this functionality. It is temporarily unavailable");
				return response;
			}
			
			
			if(!user.getId_ingo().isEmpty())
			{
				/*GetRegisteredCardsRequest Registercard = new GetRegisteredCardsRequest();
				Registercard.setCustomerId(user.getId_ingo());
				
				response = mcIngorequest.GetRegisteredCards(Registercard, deviceId);
				LOGGER.debug("RESPONSE GETREGISTEREDCARDS " + gson.toJson(response));*/
				response.setErrorCode(0);
				response.setErrorMessage("");
				response.setCards(new ArrayList<Card>());
				// response.setErrorCode(0);
			}
			else
			{
				response.setErrorCode(91);
				if(idioma.equalsIgnoreCase("es"))
					response.setErrorMessage("Para usar el servicio debe registrarse");
				else
					response.setErrorMessage("Not registered to use the service");
				response.setCards(new ArrayList<Card>());
			}
			return response;
		}catch(Exception  ex){
			response.setErrorCode(60);
			if(idioma.equalsIgnoreCase("es"))
				response.setErrorMessage("Error al procesar petición, intente mas tarde");
			else
				response.setErrorMessage("Error Processing Data");
			response.setCards(new ArrayList<Card>());
			LOGGER.error("[ERROR PROCESING] ["+ user.getId_ingo() +"]" , ex );
			return response;
		}
	}
	
	
	public UserDataEnroll EnrollCustomer(EnrollCustomerMC enrollMC, String idioma, int idApp){
		UserDataEnroll response= new UserDataEnroll();
		try{
			
			Proveedor proveedor= mapper.getProveedor("INGO");
			if(proveedor.getPrv_status()==0)
			{
				
				response.setCustomerId("");
				response.setErrorCode(503);
				if(idioma.equalsIgnoreCase("es"))
					response.setErrorMessage("Estamos mejorando esta funcionalidad. No está disponible temporalmente");
				else
					response.setErrorMessage("We are improving this functionality. It is temporarily unavailable");
				response.setSsoToken("");
				response.setSessionId("");
				return response;
			}
			
			LOGGER.debug("[INICIANDO PROCESO DE ENROLAMIENTO ] [" + enrollMC.getFirstName() + " " +enrollMC.getLastName()+"]");
			EnrollCustomerRequest request = new EnrollCustomerRequest();
			request.setAddressLine1(enrollMC.getAddressLine1() == null ? "" : enrollMC.getAddressLine1() );
			request.setAddressLine2(enrollMC.getAddressLine2() == null ? "" : enrollMC.getAddressLine2());
			request.setAllowTexts(enrollMC.getAllowTexts());
			request.setCity(enrollMC.getCity());
			request.setCountryOfOrigin(enrollMC.getCountryOfOrigin()); //optional 
			request.setDateOfBirth(enrollMC.getDateOfBirth()); //yyyy-MM-dd (e.g., 1978-09-18) 1994-07-02 00:00:00
			request.setEmail(enrollMC.getEmail());
			request.setFirstName(enrollMC.getFirstName());
			request.setGender( (enrollMC.getGender() == null || enrollMC.getGender().isEmpty()) ? "M" : enrollMC.getGender()); //Optional Single character string indicating M/F.
			request.setHomeNumber(enrollMC.getHomeNumber());
			request.setLastName(enrollMC.getLastName());
			request.setMiddleInitial(enrollMC.getMiddleInitial()); //Optional Single character middle initial. Max length is 1 character.
			request.setMobileNumber(enrollMC.getMobileNumber());
			request.setSsn(enrollMC.getSsn());
			request.setState(mapper.getState("", Integer.parseInt(enrollMC.getState()), 0).getAbreviatura());
			request.setSuffix(enrollMC.getSuffix()); // optional Typically would contain one of Jr, Sr, etc.
			request.setTitle(enrollMC.getTitle()); 
			request.setZip(enrollMC.getZip());
			
			CustomerResponse responseEnrroll =  mcIngorequest.EnrollCustomer(request, enrollMC.getDeviceId());
			LOGGER.debug("json: " + responseEnrroll.getCustomerId() + " " + responseEnrroll.getErrorMessage());
			
		    if(responseEnrroll.getCustomerId() != null && !responseEnrroll.getCustomerId().isEmpty()){
		    	mapper.updateCustomerId(enrollMC.getIdUsuario(), responseEnrroll.getCustomerId(), Integer.parseInt(enrollMC.getState()), enrollMC.getSsn(), idApp);
				
				//sesion usuario
		    	SessionRequest sessionRequest = new SessionRequest();
				AuthenticatePartnerResponse  authenticatePartnerResponse = mcIngorequest.getSession(sessionRequest, enrollMC.getDeviceId(), enrollMC.getPlataforma());
		    	
				/*if(authenticatePartnerResponse.getSessionId()== null || authenticatePartnerResponse.getSessionId().isEmpty())
				{
					response.setErrorCode(authenticatePartnerResponse.getErrorCode());
					response.setErrorMessage(authenticatePartnerResponse.getErrorMessage());
				}*/
				
				AuthenticateOBORequest authenticateOBORequest = new AuthenticateOBORequest();
				authenticateOBORequest.setCustomerId(responseEnrroll.getCustomerId());
				AuthenticateOBOResponse  authenticateOBOResponse  = mcIngorequest.AuthenticateOBO(authenticateOBORequest, enrollMC.getDeviceId());
				
				response.setCustomerId(responseEnrroll.getCustomerId());
				response.setErrorCode(authenticateOBOResponse.getErrorCode());
				response.setErrorMessage(authenticateOBOResponse.getErrorMessage());
				response.setSessionId(authenticatePartnerResponse.getSessionId());
				response.setSsoToken(authenticateOBOResponse.getSsoToken());
				response.setAddress(enrollMC.getAddressLine1());
				response.setCity(enrollMC.getCity());
				response.setState(Integer.parseInt(enrollMC.getState()));
				response.setZip(enrollMC.getZip());
				
		    }else
		    {
		    	response.setCustomerId("");
		    	response.setErrorCode(100);
		    	if(idioma.equalsIgnoreCase("es"))
		    		response.setErrorMessage(responseEnrroll.getErrorMessage());
		    	else
		    		response.setErrorMessage(responseEnrroll.getErrorMessage());
		    	response.setSessionId("");
		    	response.setSsoToken("");
		    }
			
			return response;

			
		}catch(Exception ex){
			LOGGER.error("EnrollCustomer error " + ex.getMessage(), ex);
			response.setCustomerId("");
	    	response.setErrorCode(101);
	    	if(idioma.equalsIgnoreCase("es"))
	    		response.setErrorMessage("Error inesperado, intente mas tarde");
	    	else
	    		response.setErrorMessage("Unexpected error, try later");
	    	response.setSessionId("");
	    	response.setSsoToken("");
			return response;
		}
		
	}
	
	
	public Response AddOrUpdateCard(AddOrUpdateCardRequestMC requestMC, String idioma, int idApp){
		
		Response response = new Response();
		try{
			LOGGER.debug("[INICIANDO PROCESO AGREGAR TARJETA INGO]" + "[" + requestMC.getCustomerId() + "]");
			
			Proveedor proveedor= mapper.getProveedor("INGO");
			if(proveedor.getPrv_status() == 0){
				
				response.setErrorCode(503);
				response.setErrorMessage("We are improving this functionality. It is temporarily unavailable");
				return response;
			}
			
			int id_state;
			
			if(Integer.parseInt(requestMC.getState()) == 0)
				id_state = mapper.getUserByIdCustomer(requestMC.getCustomerId(), idApp).getUsr_id_estado().intValue();
			else
				id_state = Integer.parseInt(requestMC.getState());
			
			
			Estados state = mapper.getState("",id_state ,0);
			
			AddOrUpdateCardRequest request = new AddOrUpdateCardRequest();
			request.setAddressLine1(requestMC.getAddressLine1());
			request.setAddressLine2(requestMC.getAddressLine2());
			request.setCardNickname(requestMC.getCardNickname());
			request.setCardNumber(AddcelCrypto.decryptHard(requestMC.getCardNumber())); //AddcelCrypto.decryptSensitive(
			request.setCity(requestMC.getCity());
			request.setCustomerId(requestMC.getCustomerId());
			request.setExpirationMonthYear(AddcelCrypto.decryptHard(requestMC.getExpirationMonthYear()).replace("/", "")); //month/year
			request.setNameOnCard(requestMC.getNameOnCard()); //The name of the user found on the front of the card
			request.setState(state.getAbreviatura());
			request.setZip(requestMC.getZip());
			
			LOGGER.debug("REQUEST INGO[ADDCARD]  "+ gson.toJson(request) );
			CardsResponse responseaddCard = mcIngorequest.AddOrUpdateCard(request, requestMC.getDiviceId());
			LOGGER.debug("RESPUESTA INGO[ADDCARD]: " + gson.toJson(responseaddCard));
			if(responseaddCard.getErrorCode()==0)
			{
				response.setErrorCode(0);
				response.setErrorMessage("");
			}else{
				response.setErrorCode(102);
				response.setErrorMessage(responseaddCard.getErrorMessage());
				/*if(idioma.equalsIgnoreCase("es"))
					response.setErrorMessage("Error al registrar tarjeta, contacte a soporte");
				else
					response.setErrorMessage("Error registering card, contact support");*/
			}
			return response;
			
		}catch(Exception ex){
			LOGGER.error("Error al Agregar o actualizar Tarjeta " + ex.getMessage(),ex);
			return null;
		}
	}
	
	
	public CustomerResponse FindCustomer (FindCustomerRequestMC findcustomer,String idioma, int idApp){
		try{
			LOGGER.debug("[INICIANDO PROCESO BUSQUEDA USUARIO EN INGO ] " + "[" + findcustomer.getSsn() + "]");
			Proveedor proveedor= mapper.getProveedor("INGO");
			if(proveedor.getPrv_status() == 0){
				CustomerResponse response = new  CustomerResponse();
				response.setCustomerId("");
				response.setErrorCode(503);
				response.setErrorMessage("We are improving this functionality. It is temporarily unavailable");
				return response;
			}
			FindCustomerRequest request = new FindCustomerRequest();
			request.setDateOfBirth(findcustomer.getDateOfBirth());
			request.setSsn(findcustomer.getSsn());
			CustomerResponse response =  mcIngorequest.FindCustomer(request, findcustomer.getDeviceId());
			return response;
			
		}catch(Exception ex){
			LOGGER.error("Se produjo un error al obtener customer " + ex.getMessage());
			return null;
		}
	}
	
	public CardsResponse GetRegisteredCards(GetRegisteredCardsRequestMC requestGet,String idioma, int idApp){
		try{
			LOGGER.debug("[INICIANDO PROCESO DE BUSQUEDA DE TARJETAS] [" + requestGet.getCustomerId() + "]");
			Proveedor proveedor= mapper.getProveedor("INGO");
			if(proveedor.getPrv_status() == 0){
				CardsResponse response = new CardsResponse();
				response.setCards(new ArrayList<Card>());
				response.setErrorCode(503);
				response.setErrorMessage("We are improving this functionality. It is temporarily unavailable");
				return response;
			}
			GetRegisteredCardsRequest request = new GetRegisteredCardsRequest();
			request.setCustomerId(requestGet.getCustomerId());
			return mcIngorequest.GetRegisteredCards(request, requestGet.getDeviceId());
		}catch(Exception ex){
			LOGGER.error("Se produjo un error al obtener tarjetas " + ex.getMessage());
			return null;
		}
	}
	
	public EstadosResponse getEstates(int idpais, int idApp,String idioma)
	{//3 usa
		try{
				List<Estados> estados = mapper.getEstates(idpais, -1, "");
				
				EstadosResponse response = new EstadosResponse();
				response.setEstados(estados);
				
				
				if(estados != null && estados.size() > 0)
				{
					response.setIdError(0);
					response.setMensajeError("");
				}
				else
				{
					response.setIdError(100);
					if(idioma.equalsIgnoreCase("es"))
						response.setMensajeError("País sin estados en catálogo");
					else
						response.setMensajeError("Country without states in catalog");
				}
				
				
				LOGGER.debug("Estados Count: " + estados.size());
				
				return response;
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER CATALOGO DE ESTADOS", ex);
			return new  EstadosResponse();
		}
	}
	
}
